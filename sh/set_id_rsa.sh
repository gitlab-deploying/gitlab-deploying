set -xe
mkdir -p ~/.ssh
cp $file_id_rsa ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
