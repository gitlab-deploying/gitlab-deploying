set -xe
time yum update -y
time python3 -m pip install --upgrade
cd $CI_PROJECT_NAME

useradd $CI_PROJECT_NAME || true
mkdir -p /home/$CI_PROJECT_NAME/$CI_PROJECT_NAME/
time python3 -m venv /home/$CI_PROJECT_NAME/$CI_PROJECT_NAME/venv
time /home/$CI_PROJECT_NAME/$CI_PROJECT_NAME/venv/bin/pip install -r requirements.txt

chown -R $CI_PROJECT_NAME.$CI_PROJECT_NAME /home/$CI_PROJECT_NAME
chown -R $CI_PROJECT_NAME.$CI_PROJECT_NAME .

systemctl stop $CI_PROJECT_NAME || true
cp /home/deploy-manager/$CI_PROJECT_NAME/$CI_PROJECT_NAME.service /etc/systemd/system/
cd /home/$CI_PROJECT_NAME/$CI_PROJECT_NAME/
ls | grep -v venv | xargs rm -rf
cd /home/deploy-manager/$CI_PROJECT_NAME
cp -a /home/deploy-manager/$CI_PROJECT_NAME/. /home/$CI_PROJECT_NAME/$CI_PROJECT_NAME/
        
systemctl daemon-reload && systemctl start $CI_PROJECT_NAME && systemctl enable $CI_PROJECT_NAME
rm -rf /home/deploy-manager/$CI_PROJECT_NAME/
