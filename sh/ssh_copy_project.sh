set -xe
ssh deploy-manager@$hosting_address -o "StrictHostKeyChecking=accept-new" "echo $deploy_manager_password | sudo -S sh -c 'rm -rf $CI_PROJECT_NAME'" </dev/null
tar cz --exclude ".git" ../$CI_PROJECT_NAME | ssh deploy-manager@$hosting_address tar xvz

